# playground3

## Execute  

* Run `stack exec -- playground3-exe` to see "We're inside the application!"
* With `stack exec -- playground3-exe --verbose` you will see the same message, with more logging.

## Run tests

`stack test`

## Running with nix

Switch nix.enable to true in stack.yaml.
Then run

```bash
nix-shell -p cachix --run "cachix use all-hies"
```

to add nix cache with HIE (this should be ran once). Then run 

```bash
nix-shell
stack run --nix
```
