module App.Setup where

import Import

-- import Control.Exception (bracket_)
import Control.Monad.Managed
import RIO.Orphans
import RIO.Process

import qualified SDL

import qualified Vulkan.Device as Vulkan

setup :: Options -> Managed App
setup options@Options{..} = do
  lo <- logOptionsHandle stderr optionsVerbose
  pc <- mkDefaultProcessContext
  lf <- managed $ withLogFunc lo

  resourceMap <- managed withResourceMap

  sizes <- managed withSdl
  case reverse (sort sizes) of
    [] -> do
      SDL.showSimpleMessageBox Nothing SDL.Information "LOL" "You have no display"
      exitFailure
    largestWidth : _rest -> do
      sdlWindow <- managed $ withSdlWindow optionsFullscreen largestWidth

      vulkanDevice <- Vulkan.withDevice sdlWindow

      -- XXX: Actual state resources are tied to context and should be more limited in scope.
      initial <- newSomeRef zero

      pure App
        { _appOptions        = options
        , _appLogFunc        = lf
        , _appProcessContext = pc
        , _appResourceMap    = resourceMap
        , _appSdlWindow      = sdlWindow
        , _appVulkanDevice   = vulkanDevice
        , _appState          = initial
        }

withSdl :: MonadUnliftIO m => ([V2 CInt] -> m c) -> m c
withSdl next = bracket_ SDL.initializeAll SDL.quit (getDisplaySizes >>= next)

withSdlWindow
  :: (MonadUnliftIO m)
  => Bool
  -> V2 CInt
  -> (SDL.Window -> m c) -> m c
withSdlWindow fullscreen size = bracket create SDL.destroyWindow
  where
    create = SDL.createWindow "playground 3" config

    config = SDL.defaultWindow
      { SDL.windowGraphicsContext = SDL.VulkanContext
      , SDL.windowMode            = mode
      , SDL.windowInitialSize     = size
      , SDL.windowBorder          = False
      , SDL.windowResizable       = False
      }

    mode =
      if fullscreen then
        SDL.Fullscreen
      else
        SDL.Windowed

getDisplaySizes :: MonadIO m => m [V2 CInt]
getDisplaySizes = do
  displays <- SDL.getDisplays
  pure $ map SDL.displayBoundsSize displays
