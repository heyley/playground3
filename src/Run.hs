module Run (run) where

import Import

import Data.Acquire (withAcquire)

import qualified SDL

import qualified Vulkan.Context as Vulkan

run :: RIO App ()
run = do
  window <- view appSdlWindow
  SDL.showWindow window

  contextLoop

contextLoop :: RIO App ()
contextLoop = do
  window <- view appSdlWindow
  dev <- view appVulkanDevice
  withAcquire (Vulkan.acquire window dev) mainLoop

  quitApp <- use asQuitApp
  if quitApp then
    logInfo "Bye!"
  else
    contextLoop

mainLoop :: VulkanContext -> RIO App ()
mainLoop vc = do
  SDL.pollEvents >>= mapM_ handleEvents

  quitApp     <- use asQuitApp     -- XXX: quit event
  quitContext <- use asQuitContext -- XXX: window resize etc.
  if quitApp || quitContext then
    logInfo "Done."
  else
    mainLoop vc

handleEvents :: SDL.Event -> RIO App ()
handleEvents event =
  case SDL.eventPayload event of
    SDL.KeyboardEvent kb ->
      handleKeyboard kb
    SDL.QuitEvent ->
      asQuitApp .= True
    _ ->
      pure ()

handleKeyboard :: SDL.KeyboardEventData -> RIO App ()
handleKeyboard SDL.KeyboardEventData{..} =
  case SDL.keysymKeycode keyboardEventKeysym of
    SDL.KeycodeEscape ->
      asQuitApp .= True
    _ ->
      pure ()
