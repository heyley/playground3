module Vulkan.Context where

import Import

import qualified RIO.Vector as Vector
import qualified SDL
import qualified Vulkan.Core10 as Vk
-- import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr

import Data.Acquire (Acquire, mkAcquire)

acquire :: SDL.Window -> VulkanDevice -> Acquire VulkanContext
acquire window dev = mkAcquire (create window dev) (destroy dev)

create :: SDL.Window -> VulkanDevice -> IO VulkanContext
create window VulkanDevice{..} = do
  commandPool <- Vk.createCommandPool _vdLogical commandPoolCI Nothing

  extent <- case Khr.currentExtent _pdSurfaceCaps of
    Vk.Extent2D ew eh | ew == maxBound, eh == maxBound -> do
      V2 w h <- SDL.get $ SDL.windowSize window
      pure $ Vk.Extent2D (fromIntegral @CInt w) (fromIntegral @CInt h)
    current ->
      pure current

  swapChain <- Khr.createSwapchainKHR _vdLogical (swapchainCI extent) Nothing

  (_res, images) <- Khr.getSwapchainImagesKHR _vdLogical swapChain
  swapViews <- for images \image ->
    Vk.createImageView _vdLogical (imageViewCI image) Nothing

  pure VulkanContext
    { _vcCommandPool = commandPool
    , _vcSwapChain   = swapChain
    , _vcSwapViews   = swapViews
    , _vcExtent      = extent
    }
  where
    PhysicalDevice{..} = _vdPhysical

    commandPoolCI = Vk.CommandPoolCreateInfo
      { flags            = zero
      , queueFamilyIndex = _vdPhysical ^. pdGraphicsQueueIx
      }

    swapchainCI extent = zero
      { Khr.surface            = _vdSurface
      , Khr.minImageCount      = minImageCount + 1
      , Khr.imageFormat        = Khr.format _pdSurfaceFormat
      , Khr.imageColorSpace    = Khr.colorSpace _pdSurfaceFormat
      , Khr.imageExtent        = extent
      , Khr.imageArrayLayers   = 1
      , Khr.imageSharingMode   = sharingMode
      , Khr.imageUsage         = Vk.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
      , Khr.queueFamilyIndices = Vector.fromList queueFamilyIndices
      , Khr.preTransform       = Khr.currentTransform _pdSurfaceCaps
      , Khr.compositeAlpha     = Khr.COMPOSITE_ALPHA_OPAQUE_BIT_KHR
      , Khr.presentMode        = _pdPresentMode
      , Khr.clipped            = True
      }
      where
        minImageCount =
          Khr.minImageCount (_pdSurfaceCaps :: Khr.SurfaceCapabilitiesKHR)

        (sharingMode, queueFamilyIndices) =
          if _vdGraphicsQ == _vdPresentQ then
            ( Vk.SHARING_MODE_EXCLUSIVE
            , []
            )
          else
            ( Vk.SHARING_MODE_CONCURRENT
            , [_pdGraphicsQueueIx, _pdPresentQueueIx]
            )

    imageViewCI image = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = Khr.format _pdSurfaceFormat
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }
      where
        subr = Vk.ImageSubresourceRange
          { Vk.aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
          , Vk.baseMipLevel   = 0
          , Vk.levelCount     = 1
          , Vk.baseArrayLayer = 0
          , Vk.layerCount     = 1
          }

destroy :: VulkanDevice -> VulkanContext -> IO ()
destroy VulkanDevice{_vdLogical} VulkanContext{..} = do
  Vk.destroyCommandPool _vdLogical _vcCommandPool Nothing

  for_ _vcSwapViews \imageView ->
    Vk.destroyImageView _vdLogical imageView Nothing

  Khr.destroySwapchainKHR _vdLogical _vcSwapChain Nothing
